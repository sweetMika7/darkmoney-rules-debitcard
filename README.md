**1. СТАТУС ДАННОГО ДОКУМЕНТА**

Данный документ является официальным юридическим документом офертой который регулирует соглашения между продавцом и покупателем товара в рамках их договорных отношений.

**2. ОПРЕДЕЛЕНИЕ ТЕРМИНОЛОГИИ**

**СОГЛАШЕНИЕ** – данный документ, который является юридическим признанием факта соглашения между сторонами.

**МАТЕРИАЛ** – совокупность предметов, образующих предмет в отношение которого происходит сделка купли-продажи, описанная данным документом.

**САЙТ** – сайт darkmoney (https://darkmoney.cc, https://y3pggjcimtcglaon.onion)

**АДМИНИСТРАЦИЯ САЙТА** – уполномоченные лица **САЙТА** - Администратор ( **darkmoney** ) и Супер-администратор ( **Luca**** Brasi**)

**АРБИТР** – назначенный и уполномоченный **АДМИНИСТРАЦИЕЙ САЙТА** лицо (пользователь) сайта, который имеет право по рассмотрению **АРБИТРАЖЕЙ** на **САЙТЕ** и вынесению решений.

**АРБИТРАЖ** – процесс рассмотрения **АРБИТРАМИ** искового заявления.

**ДРОП** -

**ДРОПОВОД** -

**3. ОПРЕДЕЛЕНИЕ СТОРОН**

(1) **СТОРОНА** – физическое, юридическое или не идентифицированное в юридическом плане лицо, как зарегистрированный полноправный пользователь **САЙТА** , который вступает в хозяйственные-финансовые отношения в рамках данного **СОГЛАШЕНИЯ**.

(2) **ПОКУПАТЕЛЬ** – **СТОРОНА** , которая на условиях платности приобретает **МАТЕРИАЛ** заявленного надлежащего качества и с предварительно определенными **СОГЛАШЕНИЕМ** и/или также дополнительными **СОГЛАШЕНИЯМИ** условиями после продажной гарантии, у **ПРОДАВЦА**.

(3) **ПРОДАВЕЦ** – **СТОРОНА** , которая на условиях платности продает **МАТЕРИАЛ** заявленного надлежащего качества и с предварительно определёнными по **СОГЛАШЕНИЮ** и/или также дополнительными **СОГЛАШЕНИЯМИ** условиями после продажной гарантии, по заказу или запросу **ПОКУПАТЕЛЯ**.

(4) **СТОРОНЫ** – **ПОКУПАТЕЛЬ** и **ПРОДАВЕЦ.**

**4. ПРАВА И ОБЯЗАННОСТИ ПОКУПАТЕЛЯ**

**5. ПРАВА И ОБЯЗАННОСТИ ПРОДАВЦА**

**6. РАСКРЫТИЕ ВНУТРЕННЕЙ ИНФОРМАЦИИ**

**7. ЭКСПЛУАТАЦИОННЫЕ РАСХОДЫ**

**8. КОМПЛЕКТНОСТЬ МАТЕРИАЛА**

**9. ПРОВЕРКА МАТЕРИАЛА ПРОДАВЦОМ**

**10. ДОСТАВКА МАТЕРИАЛА**

(1) Доставка материала – это процесс по доставке заказанного **ПОКУПАТЕЛЕМ** материала от **ПРОДАВЦА**

**11. ПРОВЕРКА МАТЕРИАЛА ПОКУПАТЕЛЕМ**

**12. ГАРАНТИИ НА МАТЕРИАЛ СО СТОРОНЫ ПРОДАВЦА**

**13. УСЛОВИЯ ПРИЗНАНИЕ МАТЕРИАЛА НЕНАДЛЕЖАЩЕГО КАЧЕСТВА**

**14. УСЛОВИЯ ПРИЗНАНИЯ МАТЕРИАЛА НЕНАДЛЕЖАЩЕГО КАЧЕСТВА КАК НЕВОЗВРАТНОГО**

**15. УСЛОВИЯ НАСТУПЛЕНИЯ ФИНАНСОВОЙ ИЛИ МАТЕРИАЛЬНОЙ ОТВЕТСТВЕННОСТИ ПО МАТЕРИАЛУ**

**16. СПЕЦИАЛЬНЫЕ ОБЯЗАТЕЛЬНЫЕ УСЛОВИЯ ПРИ ПРОДАЖЕ БЫВШЕГО В ИСПОЛЬЗОВАНИИ ИЛИ ВОССТАНОВЛЕННОГО МАТЕРИАЛА**

**17. СПЕЦИАЛЬНЫЕ УСЛОВИЯ ПРЕДОСТАВЛЕНИЯ НЕОБЯЗАТЕЛЬНЫХ ДОПОЛНИТЕЛЬНЫХ СОПУСТВУЮЩИХ УСЛУГ ПО МАТЕРИАЛУ**

**18. СПЕЦИАЛЬНЫЕ УСЛОВИЯ ПО ПРЕДОТВРАЩЕНИЮ ЗЛОУПОТРЕБЛЕНИЙ СО СТОРОНЫ ПОКУПАТЕЛЯ**

**19. СЧЕТЧИК КОЛИЧЕСТВА КРАЖ С КАРТ**

(1) Счетчик количества краж с карт **ПОКУПАТЕЛЕЙ** представляет из себя количественный статистический показатель призванный помощь **ПОКУПАТЕЛЮ** при выборе **ПРОДАВЦА**.

(2) Счетчик количества краж с карт **ПОКУПАТЕЛЕЙ** показывает количество обоснованных и/или доказанных случаев по краже **ДРОПОМ** или **ДРОПАМИ** средств с карт **ПОКУПАТЕЛЯ** купленных у **ПРОДАВЦА** и подтвержденных по решению **АРБИТРАЖА**.

(2.1) Обоснованность и доказанность факта кражи с карты определяется **АРБИТРАЖЕМ** в индивидуальном по каждому иску и случаю порядке **,** если другими правилами не определено иное.

(2.2) **АРБИТРАЖИ** в отношении **ПРОДАВЦА** принятые к рассмотрению, но разрешенные по обоюдному решению сторон или решению **АРБИТРАЖА** также учитываются.

(2.3) **АРБИТРАЖИ** в отношении **ПРОДАВЦА** которые не были приняты на рассмотрения в связи с отсутствием доказательств или клеветы не принимаются в учет.

(2.4) **АРБИТРАЖИ** в отношении **ПРОДАВЦА** по материалу картам, которые были проданы с расширенной гарантией на сохранность средств и/или гарантией материального или иного возмещения и при этом **ПРОДАВЕЦ** осуществил возмещение средств в указанном по соглашению объеме по решению **АРБИТРАЖА** или по обоюдной договоренности, не принимаются в учет.

 (3) **ПРОДАВЕЦ** обязан публиковать информацию о количестве краж с карт **ПОКУПАТЕЛЕЙ** отдельной строкой в своей рабочей теме.

(3.1) Текст с данной информацией запрещено писать мелким шрифтом.

(3.2) Текст с данной информацией запрещено писать таким образом, чтобы он сливался с другими текстом или текстами.

(3.3) Текст с данной информацией запрещено писать нестандартным цветом, который делает восприятие этой информации затрудненным или сложным.

(3.4) Текст с данной информацией запрещено публиковать в виде изображения.

(4) Сокрытие информации о количестве краж с карт **ПОКУПАТЕЛЕЙ** или ее искажение может считаться как намеренная попытка ввести **ПОКУПАТЕЛЯ** в заблуждение со стороны **ПРОДАВЦА**.

(5) **ПРОДАВЕЦ** принимает, что **АДМИНИСТРАЦИЯ** имеет исключительное право по частичной или полной приостановке, и/или блокированию коммерческой деятельности **ПРОДАВЦА** в случае несоответствия данного показателя разумным нормам и установленным правилам **САЙТА**.

(5.1) При обнаружении количества признанных и доказанных хищений или краж средств с карт **ПОКУПАТЕЛЕЙ** в количестве 4 или более за 365 дней, **АДМИНИСТРАЦИЯ** имеет право снять с **ПРОДАВЦА** статус «Проверенный продавец» или применить другой вид санкции, в случае обнаружения намеренных злоупотреблений со стороны **ПРОДАВЦА**.

(5.2) При обнаружении количества признанных и доказанных хищений или краж средств с карт **ПОКУПАТЕЛЕЙ** в количестве больше 10 или более за 365 дней и/или более 12 за весь срок коммерческой деятельности **ПРОДАВЦА** на **САЙТЕ** , **АДМИНИСТРАЦИЯ** имеет право заблокировать учетную запись **ПРОДАВЦА** или применить другой вид санкции, в случае обнаружения намеренных злоупотреблений со стороны **ПРОДАВЦА**.

**20. СЧЕТЧИК КОЛИЧЕСТВА ВОЗВРАТОВ СРЕДСТВ ЗА МАТЕРИАЛ**

(1) Счетчик количества возвратов средств за материал представляет из себя количественный статистический показатель призванный помощь **ПОКУПАТЕЛЮ** при выборе **ПРОДАВЦА**.

(2) Счетчик количества возвратов средств за материал показывает количество обоснованных случаев по возврату **ПРОДАВЦОМ** средств **ПОКУПАТЕЛЮ** за материал по решению **АРБИТРАЖА**.

(2.1) Обоснованным возвратом за материал признается - факт возврата по **АРБИТРАЖУ** средств **ПОКУПАТЕЛЮ** от **ПРОДАВЦА** при наличии достаточного количества доказательств в необходимости возврата, необходимых для начала его рассмотрения **АРБИТРАЖЕМ.**

(2.2) **АРБИТРАЖИ** в отношении **ПРОДАВЦА** принятые к рассмотрению, но разрешенные по обоюдному решению сторон или решению **АРБИТРАЖА** также учитываются.

(2.3) **АРБИТРАЖИ** в отношении **ПРОДАВЦА** которые не были приняты на рассмотрения в связи с отсутствием доказательств или клеветы не принимаются в учет.

(3) **ПРОДАВЕЦ** обязан публиковать информацию о количестве возвратов средств за материал отдельной строкой в своей рабочей теме.

(3.1) Текст с данной информацией запрещено писать мелким шрифтом.

(3.2) Текст с данной информацией запрещено писать таким образом, чтобы он сливался с другими текстом или текстами.

(3.3) Текст с данной информацией запрещено писать нестандартным цветом, который делает восприятие этой информации затрудненным или сложным.

(3.4) Текст с данной информацией запрещено публиковать в виде изображения.

(4) Сокрытие информации о количестве возвратов средств за материал или ее искажение может считаться как намеренная попытка ввести **ПОКУПАТЕЛЯ** в заблуждение со стороны **ПРОДАВЦА**.

**21. СЧЕТЧИК КОЛИЧЕСТВА ПОДАННЫХ АРБИТРАЖЕЙ НА ПРОДАВЦА**

(1) Счетчик количества арбитражей представляет из себя количественный статистический показатель призванный помощь **ПОКУПАТЕЛЮ** при выборе **ПРОДАВЦА**.

(2) Счетчик количества арбитражей показывает количество ранее открытых обоснованных арбитражей на **ПРОДАВЦА**.

(2.1) Обоснованным открытым арбитражем признается - факт открытия **АРБИТРАЖА** в отношении **ПРОДАВЦА** по его деятельности при продаже материала или оказание услуг и наличию достаточного количества доказательств нарушений при этой деятельности, необходимых для начала его рассмотрения **АРБИТРАЖЕМ.**

(2.2) **АРБИТРАЖИ** в отношении **ПРОДАВЦА** принятые к рассмотрению, но разрешенные по обоюдному решению сторон или решению **АРБИТРАЖА** также учитываются.

(2.3) **АРБИТРАЖИ** в отношении **ПРОДАВЦА** которые не были приняты на рассмотрения в связи с отсутствием доказательств или клеветы не принимаются в учет.

(3) **ПРОДАВЕЦ** обязан публиковать информацию о количестве **АРБИТРАЖЕЙ** отдельной строкой в своей рабочей теме.

(3.1) Текст с данной информацией запрещено писать мелким шрифтом.

(3.2) Текст с данной информацией запрещено писать таким образом, чтобы он сливался с другими текстом или текстами.

(3.3) Текст с данной информацией запрещено писать нестандартным цветом, который делает восприятие этой информации затрудненным или сложным.

(3.4) Текст с данной информацией запрещено публиковать в виде изображения.

(4) Сокрытие информации о количестве **АРБИТРАЖЕЙ** или ее искажение может считаться как намеренная попытка ввести **ПОКУПАТЕЛЯ** в заблуждение со стороны **ПРОДАВЦА**.

**22. ФОРС МАЖОРНЫЕ ОБСТОЯТЕЛЬСТВА**

(1) СТОРОНЫ признают, что во время совершения сделки могут происходит обстоятельства непреодолимой силы, которые могут влиять на участников **СОГЛАШЕНИЯ**.



**23. УСЛОВИЯ РАБОТЫ ЧЕРЕЗ ГАРАНТ**

**24. УСЛОВИЯ РАБОТЫ НАПРЯМУЮ БЕЗ УЧАСТИЯ ГАРАНТА**

**25. О НОРМАХ ОБЯЗАТЕЛЬНЫХ МИНИМАЛЬНЫХ РЕЗЕРВОВ НА ДЕПОЗИТЕ ДЛЯ ПРОДАВЦОВ**

(1) Каждый **ПРОДАВЕЦ** в категории Дебетовые карты обязан иметь **ДЕПОЗИТ** на сайте для покрытия рисков по сделкам совершаемых между ним и **ПОКУПАТЕЛЕМ**.

(2) Установить минимальный размер **ДЕПОЗИТА** для **ПРОДАВЦА** исходя из формулы: = (20% x максимальная цена за карту) x (2 + Количество краж с карт x 2)

(2.1) Данное правило устанавливает лишь базовое минимальное значение, если другие правила или положения устанавливают значение на более высоком уровне, то они будут иметь большую силу нежели это правило.

(2.2) **ПРОДАВЕЦ** сам обязуется следить за соответствием своего депозита в размере не менее чем минимальные нормы, установленные этим правилом, если другими правилами не определено иное.

 (3) **ДЕПОЗИТ ПРОДАВЦА** может быть использован для покрытия ущерба, причинённого **ПРОДАВЦОМ** в отношении **ПОКУПАТЕЛЯ** в результате неисполнения или некачественного исполнения услуг и/или непредоставление или предоставления **МАТЕРИАЛА** ненадлежащего качества, по подтвержденным заключенным сделкам или иным случаям по решению **АРБИТРАЖА**.

(3.1) Подтвержденной сделкой считается только сделка, условия которой могут быть подтверждены с помощью доказательной базы в письменном или электронном виде.

(3.2) Данный пункт можно считать недействительным, если он вступает в конфликт с другими правилами форума.

**26. ПРИМЕНИМОЕ ПРАВО**

(1) **СТОРОНЫ** признают, что если какой-то из пунктов правил признан недействительным в силу тех или иных причин, то это не отменяет юридическую силу остальных условий или всего соглашения.

(2) **СТОРОНЫ** признают, что **АДМИНИСТРАЦИЯ САЙТА** имеет исключительное безотзывное право на однозначное толкование действующих правил и соглашений на сайте.

(3) **СТОРОНЫ** признают, что **АДМИНИСТРАЦИЯ САЙТА** имеет исключительное безотзывное право на изменение содержания данного соглашения в одностороннем порядке.

(3.1) Любые изменения соглашения влияют только на вновь заключенные договора.

(3.2) Изменения в соглашении не имеют обратной силы, если иными правилами не определено иное.

**27. ДОПОЛНИТЕЛЬНЫЕ УСЛОВИЯ И СОГЛАШЕНИЯ**

(1) **ПРОДАВЕЦ** вправе устанавливать дополнительные правила и условия по согласованию с **ПОКУПАТЕЛЕМ**.

(1.1) **ПРОДАВЕЦ** не имеет право устанавливать в соглашении дополнительные правила и/или условия, которые вступают в конфликт с основными правилами **САЙТА** или основным **СОГЛАШЕНИЕМ**.

(1.2) **ПРОДАВЕЦ** не имеет право изменить в **СОГЛАШЕНИИ** дополнительные правила и/или условия, после того как **СОГЛАШЕНИЕ** с **ПОКУПАТЕЛЕМ** было заключено.

(1.3) Если **ПРОДАВЕЦ** установил в **СОГЛАШЕНИИ** дополнительные правила и/или условия, которые вступают в конфликт с основными правилами **САЙТА** или основным **СОГЛАШЕНИЕМ** – то эти дополнительные правила и/или условия считаются ничтожными и не имеют какой-либо юридической силы.

(1.4) **ПРОДАВЕЦ** обязан при установлении дополнительных правил и/или условий публиковать их в публичном доступе и доступном для **ПОКУПАТЕЛЯ** виде.

(2) **ПОКУПАТЕЛЬ** вправе установить дополнительные условия по качеству **МАТЕРИАЛА** и общих условий работы по согласованию с **ПРОДАВЦОМ**.

(2.1) **ПОКУПАТЕЛЬ** обязан зафиксировать дополнительные условия в виде, доступном для последующей проверки договоренностей.

(2.2) Если **ПОКУПАТЕЛЬ** установил в **СОГЛАШЕНИИ** дополнительные правила и/или условия, которые вступают в конфликт с основными правилами **САЙТА** или основным **СОГЛАШЕНИЕМ** – то эти дополнительные правила и/или условия считаются ничтожными и не имеют какой-либо юридической силы.